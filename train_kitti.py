#!/usr/bin/env python

from __future__ import print_function
import argparse
import numpy as np
import os
import math
import code
import sys
import cPickle as pickle
import time
import shutil

import chainer
import chainer.functions as F
import chainer.links as L
from chainer import cuda
from chainer import optimizers
from chainer import serializers

import prednet
import data_utils
from path_settings import *

cwd = os.getcwd()
rng_seed = 9876

parser = argparse.ArgumentParser()
parser.add_argument('--batchsize',       '-b', default=4,   type=int, help='batch size')
parser.add_argument('--initmodel',       '-m', default='',            help='Initialize the model from given file')
parser.add_argument('--resume',          '-r', default='',            help='Resume the optimization from snapshot')
parser.add_argument('--model_dir',       '-a', default=0.0,           help='directory where you can find a file storing model information')
parser.add_argument('--gpu',             '-g', default=-1,  type=int, help='GPU ID (negative value indicates CPU)')
parser.add_argument('--rng_seed',        '-e', default=rng_seed, type=int, help='seed for random number generator')
args = parser.parse_args()

batch_size = args.batchsize
xp = cuda.cupy if args.gpu >= 0 else np
#path = '/home/evan/samba/prednet/chainer_sandbox/vanilla_v1_singleconv'
#shutil.copyfile(path+'/train_kitti.py',  path+'/prev'+'/train_kitti.py')
#shutil.copyfile(cwd+'/data_utils.py',   subdir+'/data_utils.py')
#shutil.copyfile(path+'/prednet.py',      path+'/prev'+'/prednet.py')
#shutil.copyfile(path+'/convlstm.py', path+'/prev'+'/convlstm.py')

#shutil.copyfile(cwd+'/evaluate_kitti.py',  subdir+'/evaluate_kitti.py')


xp.random.seed(args.rng_seed)
np.random.seed(args.rng_seed)

print('rng_seed = ',rng_seed)

height = 128
width = 160
n_channels = [3, 48, 96, 192]

nt = 10
n_epochs = 400
samples_per_epoch = 1000
N_seq_val = 100

time_loss_weights = 1./ (nt - 1) * np.ones((nt))
time_loss_weights[0] = 0
model = prednet.PredNet(height,  width, n_channels)
if args.gpu >= 0:
    cuda.get_device(args.gpu).use()
    model.to_gpu()

#optimizer_condition0  = 'optimizer = optimizers.Adam(alpha=0.01,   beta1=0.9, beta2=0.999, eps=1e-08)
optimizer_condition0  = 'optimizer = optimizers.Adam(alpha=0.001,   beta1=0.9, beta2=0.999, eps=1e-08)'
optimizer_condition1  = 'optimizer = optimizers.Adam(alpha=0.0001,   beta1=0.9, beta2=0.999, eps=1e-08)' # default
optimizer_condition2  = 'optimizer = optimizers.Adam(alpha=0.00001,  beta1=0.9, beta2=0.999, eps=1e-08)'
optimizer_condition3  = 'optimizer = optimizers.Adam(alpha=0.000001, beta1=0.9, beta2=0.999, eps=1e-08)'

print(optimizer_condition0)
exec(optimizer_condition0)
#print(optimizer_condition1)
#exec(optimizer_condition1)
#print(optimizer_condition2)
#exec(optimizer_condition2)
#print(optimizer_condition3)
#exec(optimizer_condition3)

optimizer.setup(model)
#code.interact(local=dict(globals(), **locals()))
#sys.exit()
# Init/Resume
"""
if args.initmodel:
    print('Load model from', args.initmodel)
    os.chdir(cwd+'/' + args.model_dir + '/results')
    serializers.load_npz(args.initmodel, model)
    os.chdir(subdir)
if args.resume:
    print('Load optimizer state from', args.resume)
    os.chdir(cwd+'/' + args.model_dir + '/results')
    serializers.load_npz(args.resume, optimizer)
    os.chdir(subdir)
os.chdir(subdir)
"""

print('training data: load()')
data_train, data_val, source_train, source_val = data_utils.load_data()

possible_starts_train = data_utils.get_possible_start_times(source_train, nt)
possible_starts_train = np.random.permutation(possible_starts_train)
x_train = data_utils.create_batches(data_train, possible_starts_train, batch_size, nt, samples_per_epoch)
print('x_train.shape:', x_train.shape)
n_batches_train = x_train.shape[0]

possible_starts_val = data_utils.get_possible_start_times(source_val, nt)
possible_starts_val = np.random.permutation(possible_starts_val)
x_val = data_utils.create_batches(data_val, possible_starts_val, batch_size, nt, N_seq_val)
print('x_val.shape:  ', x_val.shape)
print('-------------------------------------------------------------')

loss_history = np.zeros((n_epochs, 2), dtype=np.float32)
for epoch in xrange(1, n_epochs + 1, 1):
    t0 = time.time()
    
    # create mini-batches for training    
    possible_starts_train = np.random.permutation(possible_starts_train)
    x_train = data_utils.create_batches(data_train, possible_starts_train, batch_size, nt, samples_per_epoch)
    # training loop
    loss_train_data = 0
    for i in xrange(x_train.shape[0]):
        input_data = x_train[i].reshape(batch_size, nt, x_train.shape[2], x_train.shape[3], x_train.shape[4])
        model.zerograds()
        accum_loss_train = 0
        for t in xrange(nt):
            x_now  = chainer.Variable(xp.asarray(input_data[:, t, :, :, :]).astype(xp.float32), volatile='off')
            loss_train, y_pred   = model(x_now) 
            accum_loss_train += F.sum(loss_train)*time_loss_weights[t]
            loss_train_data  += F.sum(loss_train).data*time_loss_weights[t]
        accum_loss_train.backward()
        accum_loss_train.unchain_backward()       # truncate
        optimizer.update()
        model.reset_state()
        


    # validation loop
    loss_val_data = 0
    for i in xrange(x_val.shape[0]):
        input_data = x_val[i].reshape(batch_size, nt, x_val.shape[2], x_val.shape[3], x_val.shape[4])
        
        accum_loss_val = 0
        for t in xrange(nt):
            x_now  = chainer.Variable(xp.asarray(input_data[:, t, :, :, :]).astype(xp.float32), volatile='on')
            loss_val, y_pred   = model(x_now)
            loss_val_data  += F.sum(loss_val).data*time_loss_weights[t]

        model.reset_state()
        
    loss_history[epoch -1, 0] = cuda.to_cpu(loss_train_data)/x_train.shape[0]
    loss_history[epoch -1, 1] = cuda.to_cpu(loss_val_data)  /x_val.shape[0]
    print('epoch: {0:d}  training loss: {1:>9.6f}    val loss: {2:>9.6f}    time: {3:>6.2f}'.format(epoch, loss_history[epoch -1, 0], loss_history[epoch -1, 1], time.time()-t0))
    


if not os.path.exists(MODEL_DIR): os.makedirs(MODEL_DIR)

print('save loss history')
np.save(MODEL_DIR + 'loss_history.npy', loss_history)
print('save the model')
serializers.save_npz(MODEL_DIR + 'prednet.model', model)
print('save the optimizer')
serializers.save_npz(MODEL_DIR + 'prednet.state', optimizer) 

     
        




