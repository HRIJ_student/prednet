import code
import sys

import numpy
import chainer
import chainer.functions as F
import chainer.links as L

class ConvLSTM(chainer.Chain):
    
    def __init__(self, in_channels, out_channels, height, width,
                 wscale=1, nobias=False,
                 initialW=None, initial_bias=None):
        super(ConvLSTM, self).__init__(
            # upward, lateral connections
            X_conv=L.Convolution2D(in_channels,  out_channels*4, 3, nobias=True, pad=1),
            h_conv=L.Convolution2D(out_channels, out_channels*4, 3, nobias=True, pad=1),

            bias=L.Bias(shape=(out_channels*4, height, width)),

        )
        self.in_channels  = in_channels
        self.out_channels = out_channels

        """
        if nobias:
            self.b = None
        else:
            self.add_param('b', (self.out_channels*4,height,width))
            if initial_bias is None:
                initial_bias = numpy.zeros((self.out_channels*4,height,width))
            self.b.data[...] = initial_bias
        """
        
        self.reset_state()
        
    def reset_state(self):
        self.c = self.h = None
            
    def __call__(self, x): 
        batchsize, in_channels, height, width = x.data.shape

        if self.c is None:
            self.c = chainer.Variable(self.xp.zeros(
                (batchsize,self.out_channels,height,width), 
                 dtype=x.data.dtype), volatile='auto')

        h = self.X_conv(x) # h.shape = (4, 768, 16, 20)

        if self.h is not None:
            h += self.h_conv(self.h)
            

        h = self.bias(h)

        # split conv into LSTM gates
        f, i, o, g = F.split_axis(h, 4, 1) # (4,192,16,20)              
        

        self.c = F.sigmoid(f) * self.c + F.sigmoid(i) * F.tanh(g)
        self.h = F.sigmoid(o) * F.tanh(self.c)

        #code.interact(local=dict(globals(), **locals()))
        #sys.exit()
        return self.h
                    
